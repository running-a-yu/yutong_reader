package com.imooc.reader.service;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;

import javax.annotation.Resource;

import static org.junit.Assert.*;
//依托springIOC容器运行
@RunWith(SpringJUnit4ClassRunner.class) //JUnit4运行时自动初始化IOC容器
@ContextConfiguration(locations = {"classpath:applicationContext.xml"})  //配置文件的位置，IOC容器根据配置文件完成初始化
public class TestServiceTest {
    @Resource //注入TestService对象
    private TestService testService;
    @Test
    public void batchImport() {
        testService.batchImport();
        System.out.println("批量导入成功");
    }
}