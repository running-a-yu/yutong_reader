package com.imooc.reader.utils;

import org.apache.commons.codec.digest.DigestUtils;

//MD5摘要处理
public class MD5Utils {
    public static String md5Digest(String source, Integer salt){
        //字符串的每个字符存在数组
        char[] ca = source.toCharArray();
        //混淆源数据
        for(int i=0;i<ca.length;i++){
            ca[i] = (char) (ca[i]+salt);
        }
        String target = new String(ca);
        String md5 = DigestUtils.md5Hex(target);
        return md5;
    }
}
