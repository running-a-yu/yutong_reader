package com.imooc.reader.controller.management;

import com.baomidou.mybatisplus.core.metadata.IPage;
import com.imooc.reader.entity.Book;
import com.imooc.reader.entity.Evaluation;
import com.imooc.reader.service.BookService;
import com.imooc.reader.service.exception.BussinessException;
import org.jsoup.Jsoup;
import org.jsoup.nodes.Document;
import org.jsoup.nodes.Element;
import org.springframework.http.HttpRequest;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.multipart.MultipartFile;
import org.springframework.web.servlet.ModelAndView;

import javax.annotation.Resource;
import javax.servlet.http.HttpServletRequest;
import java.io.File;
import java.io.IOException;
import java.net.MalformedURLException;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.HashMap;
import java.util.Map;

@Controller
@RequestMapping("/management/book")
public class MBookController {
    @Resource
    private BookService bookService;
    @GetMapping("/index.html")
    public ModelAndView showBook(){
        return new ModelAndView("/management/book");
    }
    //之所以使用Map，wangEditor需要返回JSON字符串代表服务器是否处理成功
    //需要接收wangEditor传来的文件，接收文件用MutipartFile接收来自客户端的数据
    // @RequestParam必须与图片上传参数相同
    //原生的请求对象HttpRequest，因为文件上传以后都要保存在一个具体的目录下

    /**
     * wangEditor文件上传
     * @param file 上传文件
     * @param request 原生请求对象
     * @return
     * @throws IOException
     */
    @PostMapping("/upload")
    @ResponseBody //JSON序列化输出
    public Map upload(@RequestParam("img")MultipartFile file, HttpServletRequest request) throws IOException {
        //得到上传目录
        String uploadPath = request.getServletContext().getResource("/").getPath()+"/upload/";
        //注意此句在Web应用运行时才会执行，
        //指向out/yutong_reader_Web_exploded或者target

        //文件名
        String fileName = new SimpleDateFormat("yyyyMMddHHmmssSSS").format(new Date());
        //原始文件扩展名
        String suffix = file.getOriginalFilename().substring(file.getOriginalFilename().lastIndexOf("."));
        //保存文件到upload
        file.transferTo(new File(uploadPath+fileName+suffix));
        //
        Map result = new HashMap();
        result.put("errno",0);
        result.put("data",new String[]{"/upload/"+fileName+suffix});
        return result;
    }

    /**
     *
     * @param book
     * @return
     */
    @PostMapping("/create")
    @ResponseBody //JSON序列化字符串
    public Map createBook(Book book){
        Map result = new HashMap();
        try {
            book.setEvaluationQuantity(0);
            book.setEvaluationScore(0f);
            Document doc = Jsoup.parse(book.getDescription());//解析图书详情
            //获取图书详情第一个图的元素对象
            Element img = doc.select("img").first();//选择器，img为html中的标签，选中所有的img标签
            String cover = img.attr("src"); //attr()获取当前元素指定的属性值
            book.setCover(cover);//来自于description描述的第一幅图
            bookService.createBook(book);
            result.put("code","0");
            result.put("msg","success");
        }catch (BussinessException ex){
            ex.printStackTrace();
            result.put("code",ex.getCode());
            result.put("msg",ex.getMsg());
        }
        return  result;
    }
    @GetMapping("/list")
    @ResponseBody
    public Map list(Integer page, Integer limit){
        if(page == null){
            page = 1;
        }
        if(limit == null){
            limit = 10;
        }
        IPage<Book> pageObject = bookService.paging(null,null,page,limit);
        Map result = new HashMap();
        result.put("code","0");
        result.put("msg","success");
        result.put("data",pageObject.getRecords());//当前页面数据
        result.put("count",pageObject.getTotal());//未分页时记录总数
        return result;
    }
    @GetMapping("/id/{id}") //与前台ajax提交时保持一致 {id}为路径变量
    @ResponseBody
    public Map selectById(@PathVariable("id") Long bookId){ //@PathVariable("id")获取路径变量
        Book book = bookService.selectById(bookId);
        Map result = new HashMap();
        result.put("code","0");
        result.put("msg","success");
        result.put("data",book);//客户端回填数据时为json.data.bookName等，服务器返回的数据中data对应图书的数据，故序列化生成时必须也为data
        return result;
    }

    /**
     * 更新图书数据
     * @param book
     * @return
     */
    @PostMapping("/update")
    @ResponseBody
    public Map updateBook(Book book){
        Map result = new HashMap();
        try {
            //更新数据时先获取原始数据
            Book rawBook = bookService.selectById(book.getBookId());
            rawBook.setBookName(book.getBookName());
            rawBook.setSubTitle(book.getSubTitle());
            rawBook.setAuthor(book.getAuthor());
            rawBook.setCategoryId(book.getCategoryId());
            rawBook.setDescription(book.getDescription());
            Document doc = Jsoup.parse(book.getDescription()); //获取html序列
            String cover = doc.select("img").first().attr("src");
            rawBook.setCover(cover);
            bookService.updateBook(rawBook);
            result.put("code","0");
            result.put("msg","success");
        }catch (BussinessException ex){
            ex.printStackTrace();
            result.put("code",ex.getCode());
            result.put("msg",ex.getMsg());
        }
        return result;
    }

    /**
     * 删除图书
     * @param bookId
     * @return
     */
    @GetMapping("/delete/{id}")
    @ResponseBody
    public Map deleteBook(@PathVariable("id") Long bookId){
        Map result = new HashMap();
        try {
            bookService.deleteBook(bookId);
            result.put("code","0");
            result.put("msg","success");
        }catch (BussinessException ex){
            ex.printStackTrace();
            result.put("code",ex.getCode());
            result.put("msg",ex.getMsg());
        }
        return result;
    }
    @GetMapping("/delete/{evaluation}")
    @ResponseBody
    public Map deleteEvaluation(@PathVariable("evaluation") Evaluation evaluation){
        Map result = new HashMap();
        return null;
    }
}
