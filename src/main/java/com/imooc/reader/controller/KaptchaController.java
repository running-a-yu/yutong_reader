package com.imooc.reader.controller;

import com.google.code.kaptcha.Producer;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.GetMapping;

import javax.annotation.Resource;
import javax.imageio.ImageIO;
import javax.servlet.ServletOutputStream;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.awt.*;
import java.awt.image.BufferedImage;
import java.io.IOException;

@Controller
public class KaptchaController {
    @Resource
    private Producer kaptchaProducer;
    //属性名与beanId保持一致
    //包含两个原生对象，SpringMVC底层依赖于J2EE的WEB模块Servlet，
    // 在实际开发中有一些特殊场景必须要使用到原生的请求或响应对象，将原生对象装在参数列表中，
    // 在程序运行时由SpringIOC容器会将当前的请求与响应对象动态地注入到对应的参数中
    //验证码组件在设计时没有考虑到SpringMVC的集成，所以必须使用原生的请求和原生的响应，因此将请求和响应放入参数列表
    @GetMapping("/verify_code")
    public void createVerifyCode(HttpServletRequest request, HttpServletResponse response) throws IOException {
        //告诉浏览器不对本次返回的图片做任何缓存
        //响应立即过期
        response.setDateHeader("Expires",0);
        //不缓存任何图片数据
        response.setHeader("Cache-Control","no-store,no-cache,must-revalidate");
        response.setHeader("Cache-Control","post-check=0,pre-check=0");
        response.setHeader("Pragma","no-cache");
        //浏览器当做图片显示
        response.setContentType("image/png");
        //将验证码保存到当前会话Session，设置请求会话属性Attribute
        //生成验证码字符文本
        String verifyCode = kaptchaProducer.createText();
        request.getSession().setAttribute("kaptchaVerifyCode",verifyCode);
        System.out.println(request.getSession().getAttribute("kaptchaVerifyCode"));
        //以字符串verifyCode创建验证码图片，图片为二进制，用输出流，若为字符则为getwriter
        BufferedImage image = kaptchaProducer.createImage(verifyCode);
        ServletOutputStream out = response.getOutputStream();
        //输出图片流，将图片从服务器端通过响应发送给客户端浏览器，查看到类型为image/png便当做图片进行展示
        ImageIO.write(image,"png",out);
        out.flush();//立即输出
        out.close();//关闭输出流
    }
}
