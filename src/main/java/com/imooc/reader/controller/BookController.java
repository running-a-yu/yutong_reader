package com.imooc.reader.controller;

import com.baomidou.mybatisplus.core.metadata.IPage;
import com.imooc.reader.entity.*;
import com.imooc.reader.service.BookService;
import com.imooc.reader.service.CategoryService;
import com.imooc.reader.service.EvaluationService;
import com.imooc.reader.service.MemberService;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.servlet.ModelAndView;

import javax.annotation.Resource;
import javax.servlet.http.HttpSession;
import java.util.List;

@Controller //控制器
public class BookController {
    @Resource
    private CategoryService categoryService; //分类的数据
    @Resource
    private BookService bookService;
    @Resource
    private EvaluationService evaluationService;
    @Resource
    private MemberService memberService;

    /**
     * 显示首页
     * @return
     */
    @GetMapping("/first")   //与url进行绑定，选定默认的根路径
    public ModelAndView showIndex(){
        ModelAndView mav = new ModelAndView("/index");
        List<Category> categoryList = categoryService.selectAll();//获取所有分类数据
        mav.addObject("categoryList",categoryList); //属性名,值
        return mav; //查询出来的分类数据可以和模板index组合实现内容的输出
    }

    /**
     * 分页查询图书列表
     * @param p 页号
     * @return 分页对象
     */
    @GetMapping("/books")
    @ResponseBody //使用springmvc对IPage对象进行JSON序列化输出，服务器端的数据绑定
    public IPage<Book> selectBook(Long categoryId, String order,Integer p){
        if(p==null){
            p=1;//默认查询第一页
        }
        IPage<Book> pageObject = bookService.paging(categoryId,order,p,10);
        return pageObject;
    }

    @GetMapping("/book/{id}") //通过路径变量{id}获取url中的图书编号，根据登录状态和会员编号查询阅读状态
    public ModelAndView showDetail(@PathVariable("id") Long id, HttpSession session){ //在session中提取当前登录用户的信息
        Book book = bookService.selectById(id);
        List<Evaluation> evalutionList = evaluationService.selectByBookId(id);
        Member member = (Member)session.getAttribute("loginMember"); //没有登录时loginMember为null，登录后loginMember存在
        ModelAndView mav = new ModelAndView("/detail");//跳转到/detail的freemarker页面
        if(member!=null){
            //获取会员阅读状态，memberReadState可能为null
            MemberReadState memberReadState = memberService.selectMemberReadState(member.getMemberId(),id);
            mav.addObject("memberReadState",memberReadState);
        }
        mav.addObject("book",book);//将当前查询出来的对象放入到ModeAndView中
        mav.addObject("evaluationList",evalutionList);
        return mav;
    }
}
