package com.imooc.reader.controller;

import com.imooc.reader.entity.Evaluation;
import com.imooc.reader.entity.Member;
import com.imooc.reader.service.MemberService;
import com.imooc.reader.service.exception.BussinessException;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.servlet.ModelAndView;

import javax.annotation.Resource;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import java.util.HashMap;
import java.util.Map;

@Controller
public class MemberController {
    @Resource
    private MemberService memberService;
    @GetMapping("/register.html")
    public ModelAndView showRegister(){
        return new ModelAndView("/register");
    }
    @GetMapping("login.html")
    public ModelAndView showLogin(){
        return new ModelAndView("/login");
    }
    //注册界面处理
    @PostMapping("/registe")
    @ResponseBody //返回Map，直接提供JSON序列化的输出
    public Map registe(String vc, String username, String password, String nickname, HttpServletRequest request) {
        //获取正确验证码，获取请求的会话Session属性Attribute
        String verifyCode = (String)request.getSession().getAttribute("kaptchaVerifyCode");
        //验证码对比
        Map result = new HashMap();
        //equalsIgnoreCase不区分大小写对比字符串
        if (vc == null || verifyCode == null || !vc.equalsIgnoreCase(verifyCode)) {
            result.put("code", "VC01");
            result.put("msg", "验证码错误");
        } else {
            //处理成功返回code0，处理失败则将错误信息送往客户端
            try {
                memberService.createMember(username, password, nickname);
                result.put("code","0");
                result.put("msg","success");
            }catch(BussinessException ex){
                ex.printStackTrace();
                result.put("code",ex.getCode());
                result.put("msg",ex.getMsg());
            }
        }
        return result; //按照其内容进行JSON序列化输出
    }
    //登录界面处理
    @PostMapping("/check_login") //与前台保持一致
    @ResponseBody
    public Map checkLogin(String username, String password, String vc, HttpSession session){
        //session对象允许直接进行注入，在登录校验后，将当前的登录的会员信息存放其中，以备后续使用
        //获取正确验证码，获取请求的会话Session属性Attribute
        String verifyCode = (String)session.getAttribute("kaptchaVerifyCode");
        //验证码对比
        Map result = new HashMap();
        //equalsIgnoreCase不区分大小写对比字符串
        if (vc == null || verifyCode == null || !vc.equalsIgnoreCase(verifyCode)) {
            result.put("code", "VC01");
            result.put("msg", "验证码错误");
        }else{
            try{ //处理成功
                Member member = memberService.checkLogin(username,password);
                session.setAttribute("loginMember",member); //设置会话属性，显示当前登录用户的数据
                result.put("code","0");
                result.put("msg","success");
            }catch (BussinessException ex){ //处理失败
                ex.printStackTrace();
                result.put("code",ex.getCode());
                result.put("msg",ex.getMsg());
            }
        }
        return result;
    }

    /**
     * 更新想看/看过阅读状态
     * @param memberId 会员id
     * @param bookId 图书id
     * @param readState 阅读状态
     * @return 处理结果
     */
    @PostMapping("/update_read_state")
    @ResponseBody
    public Map updateReadState(Long memberId, Long bookId, Integer readState){
        Map result = new HashMap();
        try{
            //更新会员的阅读状态
            memberService.updateMemberReadState(memberId,bookId,readState);
            result.put("code","0");
            result.put("msg","success");
        }catch(BussinessException ex){
            ex.printStackTrace();
            result.put("code",ex.getCode());
            result.put("msg",ex.getMsg());
        }
        return result;
    }
    @PostMapping("/evaluate")
    @ResponseBody
    public Map evaluate(Long memberId, Long bookId, Integer score, String content){
        Map result = new HashMap();
        try{
            //发布新的短评
            Evaluation eva = memberService.evaluate(memberId,bookId,score,content);
            result.put("code","0");
            result.put("msg","success");
            result.put("evaluation",eva);
        }catch(BussinessException ex){
            ex.printStackTrace();
            result.put("code",ex.getCode());
            result.put("msg",ex.getMsg());
        }
        return result;
    }
    /**
     * 短评点赞
     * @param evaluationId 短评编号
     * @return 短评对象
     */
    @PostMapping("/enjoy")
    @ResponseBody
    public Map enjoy(Long evaluationId){
        Map result = new HashMap();
        try{
            //发布新的短评
            Evaluation eva = memberService.enjoy(evaluationId);
            result.put("code","0");
            result.put("msg","success");
            result.put("evaluation",eva); //返回短评对象(包含最新的点赞数)
        }catch(BussinessException ex){
            ex.printStackTrace();
            result.put("code",ex.getCode());
            result.put("msg",ex.getMsg());
        }
        return result;
    }
}
