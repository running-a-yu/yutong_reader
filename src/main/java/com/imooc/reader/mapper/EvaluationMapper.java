package com.imooc.reader.mapper;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.imooc.reader.entity.Evaluation;
//评价Mapper接口
public interface EvaluationMapper extends BaseMapper<Evaluation> {
}
