package com.imooc.reader.mapper;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.baomidou.mybatisplus.core.mapper.Mapper;
import com.imooc.reader.entity.Book;
import com.imooc.reader.entity.Member;
//会员Mapper接口
public interface MemberMapper extends BaseMapper<Member> {
}
