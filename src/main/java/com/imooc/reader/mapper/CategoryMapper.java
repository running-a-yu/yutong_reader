package com.imooc.reader.mapper;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.imooc.reader.entity.Category;

//图书分类Mapper接口，有接口，就需要有对应的xml，所以还需要在resources/mappers下创建category.xml
public interface CategoryMapper extends BaseMapper<Category> { //注意BaseMapper加入泛型

}
