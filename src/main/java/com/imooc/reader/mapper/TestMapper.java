package com.imooc.reader.mapper;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.imooc.reader.entity.Test;

//mybatis通过此接口自动生成其实现类，内部包含test测试表的增删改查方法，即为mapper接口
public interface TestMapper extends BaseMapper<Test> {
    public void insertSample();
}
