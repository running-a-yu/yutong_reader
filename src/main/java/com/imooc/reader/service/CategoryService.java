package com.imooc.reader.service;

import com.imooc.reader.entity.Category;

import java.util.List;

public interface CategoryService {
    //返回多个结果，用List
    public List<Category> selectAll();
}
