package com.imooc.reader.service.impl;

import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.baomidou.mybatisplus.core.metadata.IPage;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.imooc.reader.entity.Book;
import com.imooc.reader.entity.Evaluation;
import com.imooc.reader.entity.MemberReadState;
import com.imooc.reader.mapper.BookMapper;
import com.imooc.reader.mapper.EvaluationMapper;
import com.imooc.reader.mapper.MemberReadStateMapper;
import com.imooc.reader.service.BookService;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Propagation;
import org.springframework.transaction.annotation.Transactional;

import javax.annotation.Resource;

@Service("bookService") //beanId
@Transactional(propagation = Propagation.NOT_SUPPORTED,readOnly = true) //声明式事务的注解，默认不开启事务
public class BookServiceImpl implements BookService {
    @Resource
    private BookMapper bookMapper;
    @Resource
    private MemberReadStateMapper memberReadStateMapper;
    @Resource
    private EvaluationMapper evaluationMapper;
    /**
     * 分页查询图书
     * @param categoryId 分类编号
     * @param order 排序方式
     * @param page 页号
     * @param rows 每页记录数
     * @return 分页对象
     */
    @Override
    public IPage<Book> paging(Long categoryId, String order, Integer page, Integer rows) {
        Page<Book> p = new Page<Book>(page,rows);
        QueryWrapper<Book> queryWrapper = new QueryWrapper<Book>();
        if(categoryId != null && categoryId !=-1){ //分类编号有效
            queryWrapper.eq("category_id",categoryId);//字段名，实际值
        }
        if(order != null){
            if(order.equals("quantity")){
                queryWrapper.orderByDesc("evaluation_quantity");//以评价数量降序排列
            }else if(order.equals("score")){
                queryWrapper.orderByDesc("evaluation_score"); //以评分降序排列
            }
        }
        //BookMapper分页的核心方法，需要两个参数Page页和QueryWrapper条件构造器（用于数据筛选），返回分页对象
        IPage<Book> pageObject = bookMapper.selectPage(p,queryWrapper);
        return pageObject;
    }

    /**
     * 根据图书编号查询图书对象
     * @param bookId 图书编号
     * @return 图书对象
     */
    @Override
    public Book selectById(Long bookId) {
        Book book = bookMapper.selectById(bookId);
        return book;
    }
    /**
     * 更新图书评分/评价数量
     */
    @Override
    @Transactional
    public void updateEvaluation() {
        bookMapper.updateEvaluation();//严格按照逐级调用，controller，service，mapper
    }


    @Override
    @Transactional //写操作启用事务
    public Book createBook(Book book) {
        bookMapper.insert(book); //book主键自增，执行完insert后，由MyBatisPlus将新增的主键回填到book的Id中
        return book; //比参数中的book多了图书编号
    }
    /** 更新图书
     * @param book 新图书数据
     * @return 更新后的数据
     */
    @Override
    @Transactional
    public Book updateBook(Book book) {
        bookMapper.updateById(book);
        return book;
    }
    /**
     * 删除图书及相关数据
     * @param bookId 图书编号
     */
    @Override
    @Transactional
    public void deleteBook(Long bookId) {
        bookMapper.deleteById(bookId);
        //删除图书阅读状态
        QueryWrapper<MemberReadState> mrsQueryWrapper = new QueryWrapper<MemberReadState>();
        mrsQueryWrapper.eq("book_id",bookId);//其where子句的字段为book_id，前台传入bookId
        memberReadStateMapper.delete(mrsQueryWrapper);
        //删除图书短评
        QueryWrapper<Evaluation> evaluationQueryWrapper = new QueryWrapper<Evaluation>();
        evaluationQueryWrapper.eq("book_id",bookId);
        evaluationMapper.delete(evaluationQueryWrapper);
    }

}
