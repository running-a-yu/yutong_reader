package com.imooc.reader.service.impl;

import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.imooc.reader.entity.Category;
import com.imooc.reader.mapper.CategoryMapper;
import com.imooc.reader.service.CategoryService;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Propagation;
import org.springframework.transaction.annotation.Transactional;

import javax.annotation.Resource;
import java.util.List;

@Service("categoryService") //面向接口编程的原则（容器中实现类的beanId为其本名），在实现类上增加@Service
@Transactional(propagation = Propagation.NOT_SUPPORTED,readOnly = true) //事务传播，当前方法默认情况不适使用事务
//查询方法居多，不使用事务；写入方法居多，使用事务
public class CategoryServiceImpl implements CategoryService {
    @Resource
    private CategoryMapper categoryMapper;
//    查询所有图书分类
//    @return 图书分类List
    @Override
    public List<Category> selectAll() {
        //selectList()用于查询列表返回多个数据，传入新的无参的条件构造器，表明查询所有
        List<Category> list = categoryMapper.selectList(new QueryWrapper<Category>());
        return list;
    }
}
