package com.imooc.reader.service;

import com.baomidou.mybatisplus.core.metadata.IPage;
import com.imooc.reader.entity.Book;

/**
 * 图书服务，实现分页查询
 */
public interface BookService {
    /**
     * IPage是由Mybatisplus所提供的分页对象，包含了当前查询出来的页数据，及与分页相关的信息
     * 分页查询图书
     * @param categoryId 分类编号
     * @param order 排序方式
     * @param page 查询第几页数据
     * @param rows 每页有多少条数据
     * @return 分页对象
     */
    public IPage<Book> paging(Long categoryId, String order, Integer page, Integer rows); //加泛型，查询出的每条数据都为Book对象

    /**
     * 根据图书编号查询图书对象
     * @param bookId 图书编号
     * @return 图书对象
     */
    public Book selectById(Long bookId);
    /**
     * 更新图书评分/评价数量
     */
    public void updateEvaluation();

    /**
     * 创建新的图书
     * @param book
     * @return
     */
    public  Book createBook(Book book);

    /** 更新图书
     * @param book 新图书数据
     * @return 更新后的数据
     */
    public Book updateBook(Book book);

    /**
     * 删除图书及相关数据
     * @param bookId 图书编号
     */
    public void deleteBook(Long bookId);
}
