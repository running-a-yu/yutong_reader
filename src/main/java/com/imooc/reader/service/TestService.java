package com.imooc.reader.service;

import com.imooc.reader.entity.Test;
import com.imooc.reader.mapper.TestMapper;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import javax.annotation.Resource;
//服务类
@Service
public class TestService {
    @Resource //运行时注入TestMapper的对象
    private TestMapper testMapper;
    @Transactional //事务控制注解，当方法运行成功，则进行全局提交，当方法抛出运行时异常，则进行全局回滚
    public void batchImport(){ //完成了 要么数据全部完成，要么数据什么都不做
        //向test表中插入五条数据
        for(int i=0;i<5;i++){
//            if(i==3){ //原本要插入5条数据，但是只插入了3条，数据的完整性被破坏，需要引入声明式事务，对事务进行全局性控制
//                throw new RuntimeException("预期外异常");
//            }
            testMapper.insertSample();
        }
    }
}
