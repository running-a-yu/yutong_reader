package com.imooc.reader;

import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.imooc.reader.entity.Test;
import com.imooc.reader.mapper.TestMapper;
import org.junit.runner.RunWith;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;

import javax.annotation.Resource;
import java.util.List;

//IOC容器初始化的过程中，根据定义的映射关系自动地生成增删改查的SQL语句
@RunWith(SpringJUnit4ClassRunner.class) //按xml配置文件初始化IOC容器
@ContextConfiguration(locations = {"classpath:applicationContext.xml"})
public class MyBatisPlusTest {
    @Resource
    private TestMapper testMapper;
    @org.junit.Test
    public void testInsert(){
        Test test = new Test();
        test.setContent("MyBatis Plus测试");
        testMapper.insert(test); //insert方法为BaseMapper父接口中原有的方法
    }
    //更新某字段的content值,修改方法
    @org.junit.Test
    public void testUpdate(){
        //selectBy...都是MybatisPlus的方法
        Test test = testMapper.selectById(9);
        test.setContent("MyBatis Plus测试1");
        testMapper.updateById(test);
    }
    //删除方法
    @org.junit.Test
    public void testDelete(){
        testMapper.deleteById(9);
    }

    //查询方法
    @org.junit.Test
    public void testSelect(){
        //selectList()查询结果用List返回
        //selectOne()查询结果只有一条
        //selectById()按id进行查询
        //queryWrapper查询条件构造器
        QueryWrapper<Test> queryWrapper = new QueryWrapper<Test>();
        queryWrapper.eq("id",7); //查询id号 =7 和 >5 的结果 (equals,greater)
        queryWrapper.gt("id",5);
        List<Test> list = testMapper.selectList(queryWrapper);
        System.out.println(list.get(0));
    }
}
