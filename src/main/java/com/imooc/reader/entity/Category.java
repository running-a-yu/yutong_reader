package com.imooc.reader.entity;

import com.baomidou.mybatisplus.annotation.IdType;
import com.baomidou.mybatisplus.annotation.TableId;
import com.baomidou.mybatisplus.annotation.TableName;

//图书分类实体
@TableName("category")  //表名
public class Category { //mybatisplus的设计要求，属性要和数据库字段一一对应
    //主键生成方式，自动使用数据库底层自增编号的方式生成
    @TableId(type = IdType.AUTO)
    private Long categoryId;
    //@TableField("category_name") //符合驼峰命名转换的要求，TableField可省略
    private String categoryName;

    @Override
    public String toString() {
        return "Category{" +
                "categoryId=" + categoryId +
                ", categoryName='" + categoryName + '\'' +
                '}';
    }

    public Long getCategoryId() {
        return categoryId;
    }

    public void setCategoryId(Long categoryId) {
        this.categoryId = categoryId;
    }

    public String getCategoryName() {
        return categoryName;
    }

    public void setCategoryName(String categoryName) {
        this.categoryName = categoryName;
    }
}
