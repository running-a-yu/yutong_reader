<!DOCTYPE html>
<html lang="en"><head>
    <meta charset="UTF-8">
    <title>慕课书评网</title><!--bootstrap前端UI框架-->
    <meta name="viewport" content="width=device-width,initial-scale=1.0, maximum-scale=1.0,user-scalable=no">
    <link rel="stylesheet" href="./resources/bootstrap/bootstrap.css">
    <link rel="stylesheet" href="./resources/raty/lib/jquery.raty.css">
    <script src="./resources/jquery.3.3.1.min.js"></script>
    <script src="./resources/bootstrap/bootstrap.min.js"></script>
    <script src="./resources/art-template.js"></script>
    <script src="./resources/raty/lib/jquery.raty.js"></script>
	<!--注意加载文件的顺序问题 =~= -->
    <meta name="referrer" content="no-referrer" /> <!--img标签预加载网络图片，显示出图书图片-->
    <style>
        .highlight {
            color: red !important;
        }
        a:active{
            text-decoration: none!important;
        }
    </style>
    

    <style>
        .container {
            padding: 0px;
            margin: 0px;
        }

        .row {
            padding: 0px;
            margin: 0px;
        }

        .col- * {
            padding: 0px;
        }
    </style>
    <#--定义模板-->
    <script type="text/html" id="tpl"> <!--说明内容类型是html（浏览器不会当做JS代码解析）和模板id-->
        <!--循环地产生一块一块的超链接-->
        <a href="/book/{{bookId}}" style="color: inherit"><!--{{属性名}}-->
            <div class="row mt-2 book">
                <div class="col-4 mb-2 pr-2">
                    <img class="img-fluid" src="{{cover}}"><!--封面-->
                </div>
                <div class="col-8  mb-2 pl-0">
                    <h5 class="text-truncate">{{bookName}}</h5><!--图书名称-->

                    <div class="mb-2 bg-light small  p-2 w-100 text-truncate">{{author}}</div><!--图书作者信息-->


                    <div class="mb-2 w-100">{{subTitle}}</div><!--图书子标题-->

                    <p> <!--图书评分信息和评价数量,data-score是rety的要求,用来描述当前组件的评分-->
                        <span class="stars" data-score="{{evaluationScore}}" title="gorgeous"></span>
                        <span class="mt-2 ml-2">{{evaluationScore}}</span>
                        <span class="mt-2 ml-2">{{evaluationQuantity}}人已评</span>
                    </p>
                </div>
            </div>
        </a>
        <hr>
    </script>

<#-- 设置raty图片目录-->
    <script>
        <#-- 设置raty图片目录-->
        $.fn.raty.defaults.path ="./resources/raty/lib/images"
        //loadMore()加载更多数据
        //isReset参数设置为true,代表从第1页开始查询,否则按nextPage查询后序续页
        function loadMore(isReset){
            if(isReset == true){
                $("#bookList").html(""); //清空已有的图书数据
                $("#nextPage").val(1);
            }
            var nextPage = $("#nextPage").val();//获取下一页的页号
            //获取两个隐藏域的数值categoryId和，在发送请求时额外添加
            var categoryId = $("#categoryId").val();
            var order = $("#order").val();
            $.ajax({ //实现Ajax在freemarker进行渲染，是动态加载，在当前页面局部刷新，需要添加JS块进行请求的提交和返回数据的处理
                url : "/books" ,
                //data发送数据时，将它组织为参数发往服务器，参数名与BookController里的selectbook参数名相同
                data : {p:nextPage,"categoryId":categoryId,"order":order}, //JS解释引擎默认在键的前后加引号"p"当做字符串
                type : "get", //发送的请求类型
                dataType : "json", //服务器返回的类型
                success :function(json){ //服务器返回数据时,以function函数接收数据
                    console.info(json); //控制台打印
                    //当前页面的数据,records要和服务器返回的属性名相同
                    var list = json.records; //JS中只有数组没有集合
                    //循环往bookList添加图书
                    for(var i=0; i<list.length;i++){
                        var book = json.records[i];  //提取每个图书的信息
                        // var html = "<li>"+book.bookName+"</li>"; //字符串拼接生成html语句
                        //通过JS动态地将数据组织为html，插入到当前页面，将数据结合tpl模板,生成html
                        //tpl为模板id
                        var html = template("tpl", book);
                        console.info(html); //打印生成的html
                        $("#bookList").append(html); //jquery的id选择器选中div对象,append向当前div对象追加指定的html
                    }
                    //显示星型评价组件.stars将当前页面所有拥有.stars的span进行选中，.raty()将span转为可视的星星组件
                    $(".stars").raty({readOnly:true});
                    //判断是否到最后一页
                    if(json.current < json.pages){ //若当前页号<总页数
                        $("#nextPage").val(parseInt(json.current)+1); //避免变为字符串连接，JS解析可能会当做字符串处理
                        $("#btnMore").show(); //显示出 加载更多 的按钮
                        $("#divNoMore").hide(); //隐藏 之后没有数据的div
                    }else {
                        $("#btnMore").hide(); //隐藏 加载更多 的按钮
                        $("#divNoMore").show(); //显示 之后没有数据的div
                    }
                }
            })
        }
        //发送Ajax,与BookController的url_books进行交互,之前一定要引用jquery
        $(function (){
            // $.ajax({ //Ajax请求
            //     url : "/books" ,
            //     data : {p:1}, //JS解释引擎默认在键的前后加引号"p"当做字符串，需要与BookController里的selectbook参数名p相同
            //     type : "get", //发送的请求类型
            //     dataType : "json", //服务器返回的类型
            //     success :function(json){ //服务器返回数据时，以function函数接收数据
            //         console.info(json); //控制台打印
            //         //当前页面的数据，records要和服务器返回的属性名相同
            //         var list = json.records; //JS中只有数组没有集合
            //         //循环往bookList添加图书
            //         for(var i=0; i<list.length;i++){
            //             var book = json.records[i];  //提取每个图书的信息
            //             // var html = "<li>"+book.bookName+"</li>"; //字符串拼接生成html语句
            //             //将数据结合tpl模板（tpl为模板id），生成html
            //             var html = template("tpl", book);
            //             console.info(html); //打印生成的html
            //             $("#bookList").append(html); //jquery的id选择器选中div对象，append向当前div对象追加指定的html
            //         }
            //         //显示星型评价组件
            //         $(".stars").raty({readOnly:true});
            //     }
            // })
            loadMore(true);
        })
        //绑定加载更多按钮单击事件
        $(function (){
            $("#btnMore").click(function (){ //btnMore加载更多按钮的id click()单击事件
                loadMore();
            })
            $(".category").click(function () { //.category为每个分类超链接必然拥有的css类，移除所有高亮，置为灰色，再设置当前单击为高亮
                $(".category").removeClass("highlight");
                $(".category").addClass("text-black-50");
                $(this).addClass("highlight");
                //自定义属性data-category，得到当前点击的超链接的categoryId编号，将编号赋值给隐藏域categoryId
                //赋值后，数据加载就会读取，完成数据筛选
                var categoryId = $(this).data("category");
                $("#categoryId").val(categoryId);
                loadMore(true); //排序后重新从第一页开始
            })
            $(".order").click(function (){
                $(".order").removeClass("highlight");
                $(".order").addClass("text-black-50");
                $(this).addClass("highlight");
                var order = $(this).data("order");
                $("#order").val(order);
                loadMore(true);
            })
        })
    </script>
</head>
<body>
<div class="container">
    <nav class="navbar navbar-light bg-white shadow mr-auto">
        <ul class="nav">
            <li class="nav-item">
                <a href="https://sm.ms/image/qb7uPogwt6WU3pk" target="_blank">
                    <img src="https://s2.loli.net/2022/04/03/qb7uPogwt6WU3pk.jpg" >
                </a>
            </li>
        <!--登录超链接-->
        </ul>
            <#if loginMember??><!--loginMember存在时-->
                <h6 class="mt-1">
                    <img style="width: 2rem;margin-top: -5px" class="mr-1" src="./images/user_icon.png">${loginMember.nickname}
                </h6>
            <#else>
                <a href="/login.html" class="btn btn-light btn-sm">
                    <img style="width: 2rem;margin-top: -5px" class="mr-1" src="./images/user_icon.png">登录
                </a>
            </#if>
    </nav>
    <div class="row mt-2"><!--自定义属性data-category、data-order-->


        <div class="col-8 mt-2">
            <h4>热评好书推荐</h4>
        </div>

        <div class="col-8 mt-2">
                <span data-category="-1" style="cursor: pointer" class="highlight  font-weight-bold category">全部</span>
                |
                <#list categoryList as category>  <!--freemarker的语法，用于遍历数组，完成以下的循环-->
                <a style="cursor: pointer" data-category="${category.categoryId}" class="text-black-50 font-weight-bold category">${category.categoryName}</a>
                <#if category_has_next>|</#if> <!--freemarker的语法-->
                </#list>

        </div>

        <div class="col-8 mt-2">
            <span data-order="quantity" style="cursor: pointer" class="order highlight  font-weight-bold mr-3">按热度</span>

            <span data-order="score" style="cursor: pointer" class="order text-black-50 mr-3 font-weight-bold">按评分</span>
        </div>
    </div>
    <div class="d-none">
        <!--隐藏域，点击加载更多后查询第2页，用JS捕捉这个按钮的单击事件来查询此页码的数据-->
        <input type="hidden" id="nextPage" value="2">
        <input type="hidden" id="categoryId" value="-1">
        <input type="hidden" id="order" value="quantity">
    </div>

    <div id="bookList">

    </div>
    <button type="button" id="btnMore" data-next-page="1" class="mb-5 btn btn-outline-primary btn-lg btn-block">
        点击加载更多...
    </button>
    <div id="divNoMore" class="text-center text-black-50 mb-5" style="display: none;">没有其他数据了</div>
</div>

</body></html>