<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <title>从 0 开始学爬虫-慕课书评网</title>
    <meta name="viewport" content="width=device-width,initial-scale=1.0, maximum-scale=1.0,user-scalable=no">
    <link rel="stylesheet" href="/resources/bootstrap/bootstrap.css">
    <link rel="stylesheet" href="/resources/raty/lib/jquery.raty.css">
    <script src="/resources/jquery.3.3.1.min.js"></script>
    <script src="/resources/bootstrap/bootstrap.min.js"></script>
    <script src="/resources/art-template.js"></script>
    <script src="/resources/raty/lib/jquery.raty.js"></script>
    <meta name="referrer" content="no-referrer" /> <!--img标签预加载网络图片-->
    <style>
        .container {
            padding: 0px;
            margin: 0px;
        }

        .row {
            padding: 0px;
            margin: 0px;
        }

        .alert {
            padding-left: 0.5rem;
            padding-right: 0.5rem;
        }

        .col- * {
            padding: 0px;
        }

        .description p {
            text-indent: 2em;
        }

        .description img {
            width: 100%;
        }

        .highlight {
            background-color: lightskyblue !important;
        }

    </style>
    <script>
        $.fn.raty.defaults.path = '/resources/raty/lib/images';
        $(function () {
            $(".stars").raty({readOnly: true});
        })

        $(function (){
            <#if memberReadState ??> <!--freemarker ??表示存在-->
            //重选阅读状态
            //结合服务器端freemarker动态地在页面加载后利用JS将按钮的状态置为高亮
                $("*[data-read-state='${memberReadState.readState}']").addClass("highlight");//JS属性选择器，addClass增加指定的CSS
            </#if>
            <#if !loginMember ??> //若loginMember不存在
            //一次性对三个选择器所拥有的对象都进行单击的事件绑定
                $("*[data-read-state],*btnEvaluation,*[data-evaluation-id]").click(function (){ //只要某个标签有data-read-state自定义属性，绑定单击事件
                    //未登录情况下提示"需要登录"，前置检查
                    $("#exampleModalCenter").modal("show");//利用JQury选择器选中Modaldiv，modal()对话框函数
                })
            </#if>

            <#if loginMember ??>
                $("*[data-read-state]").click(function (){
                    var readState = $(this).data("read-state");//1为想看，2位看过
                    $.post("/update_read_state",{//向服务器发送post形式的ajax请求（简化形式）
                        memberId:${loginMember.memberId},
                        bookId:${book.bookId},
                        readState : readState
                    },function (json){
                        if(json.code == "0"){
                            $("*[data-read-state]").removeClass("highlight"); //删除原有的高亮
                            $("*[data-read-state='"+ readState +"']").addClass("highlight"); //与状态对应的想看/看过背景色调为高亮
                        }
                    },"json")
                });
                //短评对话框
                $("#btnEvaluation").click(function(){
                    $("#score").raty({}); //选中Id==score的标签，将其转换为星型组件
                    $("#dlgEvaluation").modal("show");//显示短评对话框
                })
                //短评对话框的提交按钮，评论对话框提交数据
                $("#btnSubmit").click(function (){
                    var score = $("#score").raty("score");//获取评分
                    var content = $("#content").val();//获取短评内容
                    //若用户没有进行选择 或者 短评前后删除空格后变为空(短评没有内容)，则直接返回
                    if(score == 0 || $.trim(content)==""){
                        return;
                    }
                    $.post("/evaluate",{//既选择了分数又输入了短评，则发送ajax请求
                        score : score,
                        bookId : ${book.bookId},
                        memberId : ${loginMember.memberId},
                        content : content
                    },function (json){
                        if(json.code=="0"){ //服务器处理成功
                            window.location.reload(); //刷新当前页面
                        }
                    },"json")//服务器返回的是json格式的数据
                })
                //评论点赞
                $("*[data-evaluation-id]").click(function (){
                    var evaluationId = $(this).data("evaluation-id");
                    $.post("/enjoy",{evaluationId:evaluationId},function (json){
                        if(json.code == "0"){ //服务器处理成功时，需要将点赞按钮中span保存的数字进行更新
                            //找到当前点击按钮下的span标签(按钮中span标签唯一),text()修改span的显示内容
                            $("*[data-evaluation-id='" + evaluationId + "'] span").text(json.evaluation.enjoy);//将服务器更新后新的点赞数量回填到span标签
                        }
                    })
                })
            </#if>
        })
    </script>
</head>
<body>
<!--<div style="width: 375px;margin-left: auto;margin-right: auto;">-->
<div class="container ">
    <nav class="navbar navbar-light bg-white shadow mr-auto">
        <ul class="nav">
            <li class="nav-item">
                <a href="/">
                    <img src="https://m.imooc.com/static/wap/static/common/img/logo2.png"  class="mt-1"
                         style="width: 100px">
                </a>
            </li>

        </ul>
    </nav>


    <div class="container mt-2 p-2 m-0" style="background-color:rgb(127, 125, 121)">
        <div class="row">
            <div class="col-4 mb-2 pl-0 pr-0">
                <img style="width: 110px;height: 160px"
                     src="${book.cover}">
            </div>
            <div class="col-8 pt-2 mb-2 pl-0">
                <h6 class="text-white">${book.bookName}}</h6>
                <div class="p-1 alert alert-warning small" role="alert">
                    ${book.subTitle}
                </div>
                <p class="mb-1">
                    <span class="text-white-50 small">${book.author}</span>
                </p>
                <div class="row pl-1 pr-2">
                    <div class="col-6 p-1">
                        <button type="button" data-read-state="1" class="btn btn-light btn-sm w-100">
                            <img style="width: 1rem;" class="mr-1"
                                 src="https://img3.doubanio.com/f/talion/cf2ab22e9cbc28a2c43de53e39fce7fbc93131d1/pics/card/ic_mark_todo_s.png"/>想看
                        </button>
                    </div>
                    <div class="col-6 p-1">
                        <button type="button" data-read-state="2" class="btn btn-light btn-sm  w-100">
                            <img style="width: 1rem;" class="mr-1"
                                 src="https://img3.doubanio.com/f/talion/78fc5f5f93ba22451fd7ab36836006cb9cc476ea/pics/card/ic_mark_done_s.png"/>看过
                        </button>
                    </div>
                </div>
            </div>
        </div>
        <div class="row" style="background-color: rgba(0,0,0,0.1);">
            <div class="col-2"><h2 class="text-white">${book.evaluationScore}</h2></div>
            <div class="col-5 pt-2">
                <span class="stars" data-score="${book.evaluationScore}"></span>
            </div>
            <div class="col-5  pt-2"><h5 class="text-white">${book.evaluationQuantity}人已评</h5></div>
        </div>
    </div>
    <div class="row p-2 description">
        ${book.description} <!--一系列html片段，图书描述信息-->
    </div>


    <div class="alert alert-primary w-100 mt-2" role="alert">短评
        <button type="button" id="btnEvaluation" class="btn btn-success btn-sm text-white float-right"
                style="margin-top: -3px;">
            写短评
        </button>
    </div>
    <div class="reply pl-2 pr-2">
        <#list evaluationList as evaluation>
            <div>
                <div>
                    <span class="pt-1 small text-black-50 mr-2">${evaluation.createTime?string('MM-dd')}</span><!--只保留月份和天数-->
                    <span class="mr-2 small pt-1">${evaluation.member.nickname}</span>
                    <span class="stars mr-2" data-score="${evaluation.score}"></span>

                    <button type="button" data-evaluation-id="${evaluation.evaluationId}"
                            class="btn btn-success btn-sm text-white float-right" style="margin-top: -3px;">
                        <img style="width: 24px;margin-top: -5px;" class="mr-1"
                             src="https://img3.doubanio.com/f/talion/7a0756b3b6e67b59ea88653bc0cfa14f61ff219d/pics/card/ic_like_gray.svg"/>
                        <span>${evaluation.enjoy}</span>
                    </button>
                </div>

                <div class="row mt-2 small mb-3">
                    ${evaluation.content}
                </div>
                <hr/>
            </div>
        </#list>
    </div>
</div>


<!-- Modal div -->
<div class="modal fade" id="exampleModalCenter" tabindex="-1" role="dialog" aria-labelledby="exampleModalCenterTitle"
     aria-hidden="true">
    <div class="modal-dialog modal-dialog-centered" role="document">
        <div class="modal-content">
            <div class="modal-body">
                您需要登录才可以操作哦~
            </div>
            <div class="modal-footer">
                <a href="/login.html" type="button" class="btn btn-primary">去登录</a>
            </div>
        </div>
    </div>
</div>

<!-- Modal -->
<div class="modal fade" id="dlgEvaluation" tabindex="-1" role="dialog" aria-labelledby="exampleModalCenterTitle"
     aria-hidden="true">
    <div class="modal-dialog modal-dialog-centered" role="document">
        <div class="modal-content">
            <div class="modal-body">
                <h6>为"${book.bookName}"写短评</h6>
                <form id="frmEvaluation">
                    <div class="input-group  mt-2 ">
                        <span id="score"></span>
                    </div>
                    <div class="input-group  mt-2 ">
                        <input type="text" id="content" name="content" class="form-control p-4" placeholder="这里输入短评">
                    </div>
                </form>
            </div>
            <div class="modal-footer">
                <button type="button" id="btnSubmit" class="btn btn-primary">提交</button>
            </div>
        </div>
    </div>
</div>

</body>
</html>